import app.lib.Coolpay;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;


public class CoolpayTest extends Mockito {
    @Captor ArgumentCaptor<Entity<String>> entityCaptor;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_login() throws Exception {
        Coolpay coolpay = new Coolpay("username", "key");

        Response response = mock(Response.class);
        when(response.getStatus()).thenReturn(200);
        JsonObject obj = new JsonObject();
        obj.addProperty("token", "1234");
        when(response.readEntity(String.class)).thenReturn(obj.toString());

        Invocation.Builder builder = mock(Invocation.Builder.class);
        WebTarget target = mock(WebTarget.class);
        Client client = mock(Client.class);

        when(builder.post(entityCaptor.capture())).thenReturn(response);
        when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
        when(client.target(anyString())).thenReturn(target);

        coolpay.setClient(client);
        String token = coolpay.login();
        assertEquals(token, obj.get("token").getAsString());

        JsonObject arg = new JsonParser().parse(entityCaptor.getValue().
                getEntity()).getAsJsonObject();
        assertEquals(arg.get("username").getAsString(), "username");
        assertEquals(arg.get("apikey").getAsString(), "key");
    }

    @Test
    public void test_create_recipient() throws Exception {
        Coolpay coolpay = new Coolpay("username", "key");
        coolpay.setToken("1234");
        Response response = mock(Response.class);
        when(response.getStatus()).thenReturn(201);
        JsonObject obj = new JsonObject();
        JsonObject recObj = new JsonObject();
        recObj.addProperty("id", "1234");
        recObj.addProperty("name", "Killian");
        obj.add("recipient", recObj);

        when(response.readEntity(String.class)).thenReturn(obj.toString());

        Invocation.Builder builder = mock(Invocation.Builder.class);
        WebTarget target = mock(WebTarget.class);
        Client client = mock(Client.class);
        when(builder.header(anyString(), anyString())).thenReturn(builder);
        when(builder.post(entityCaptor.capture())).thenReturn(response);
        when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
        when(client.target(anyString())).thenReturn(target);

        coolpay.setClient(client);
        Coolpay.Recipient recipient = coolpay.createRecipient("Killian");
        assertEquals(recipient.getId(), "1234");
        assertEquals(recipient.getName(), "Killian");

        JsonObject arg = new JsonParser().parse(entityCaptor.getValue().
                getEntity()).getAsJsonObject().get("recipient").getAsJsonObject();
        assertEquals(arg.get("name").getAsString(), "Killian");
    }

    @Test
    public void test_create_recipient_bad_token() throws Exception {
        Coolpay coolpay = new Coolpay("username", "key");
        coolpay.setToken("bad-token");

        //login mock
        Response loginResponse = mock(Response.class);
        when(loginResponse.getStatus()).thenReturn(200);
        JsonObject obj = new JsonObject();
        obj.addProperty("token", "good-token");
        when(loginResponse.readEntity(String.class)).thenReturn(obj.toString());

        Invocation.Builder loginBuilder = mock(Invocation.Builder.class);
        WebTarget loginTarget = mock(WebTarget.class);
        Client client = mock(Client.class);

        when(loginBuilder.post(entityCaptor.capture())).thenReturn(loginResponse);
        when(loginTarget.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(loginBuilder);
        when(client.target("https://coolpay.herokuapp.com/api/login")).thenReturn(loginTarget);

        //create recipient mock
        Response goodResponse = mock(Response.class);
        when(goodResponse.getStatus()).thenReturn(201);
        obj = new JsonObject();
        JsonObject recObj = new JsonObject();
        recObj.addProperty("id", "an-id");
        recObj.addProperty("name", "Killian");
        obj.add("recipient", recObj);

        Response badResponse = mock(Response.class);
        when(badResponse.getStatus()).thenReturn(401);


        when(goodResponse.readEntity(String.class)).thenReturn(obj.toString());

        Invocation.Builder badBuilder = mock(Invocation.Builder.class);
        Invocation.Builder goodBuilder = mock(Invocation.Builder.class);
        Invocation.Builder builder = mock(Invocation.Builder.class);
        WebTarget target = mock(WebTarget.class);
        when(builder.header(anyString(), eq("Bearer bad-token"))).thenReturn(badBuilder);
        when(builder.header(anyString(), eq("Bearer good-token"))).thenReturn(goodBuilder);
        when(badBuilder.post(entityCaptor.capture())).thenReturn(badResponse);
        when(goodBuilder.post(entityCaptor.capture())).thenReturn(goodResponse);
        when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
        when(client.target("https://coolpay.herokuapp.com/api/recipients")).thenReturn(target);

        coolpay.setClient(client);
        Coolpay.Recipient recipient = coolpay.createRecipient("Killian");
        assertEquals(recipient.getId(), "an-id");
        assertEquals(recipient.getName(), "Killian");
        assertEquals(coolpay.getToken(), "good-token");

        JsonObject arg = new JsonParser().parse(entityCaptor.getValue().
                getEntity()).getAsJsonObject().get("recipient").getAsJsonObject();
        assertEquals(arg.get("name").getAsString(), "Killian");
    }



    @Test(expected = Coolpay.EntityError.class)
    public void test_create_payment_invalid_recipient() throws Exception {
        Coolpay coolpay = new Coolpay("username", "key");
        coolpay.setToken("token");
        Response response = mock(Response.class);
        when(response.getStatus()).thenReturn(422);

        JsonObject obj = new JsonObject();
        JsonObject errObj = new JsonObject();
        JsonArray recArr = new JsonArray();
        recArr.add("does not exist");
        errObj.add("recipient_id", recArr);
        obj.add("errors", errObj);

        when(response.readEntity(String.class)).thenReturn(obj.toString());

        Invocation.Builder builder = mock(Invocation.Builder.class);
        WebTarget target = mock(WebTarget.class);
        Client client = mock(Client.class);
        when(builder.header(anyString(), anyString())).thenReturn(builder);
        when(builder.post(entityCaptor.capture())).thenReturn(response);
        when(target.request(MediaType.APPLICATION_JSON_TYPE)).thenReturn(builder);
        when(client.target(anyString())).thenReturn(target);

        try {
            coolpay.setClient(client);
            Coolpay.Payment recipient = coolpay.createPayment(
                    "a", new BigDecimal(10), "GBP");
        } catch (Coolpay.EntityError e) {
            assertEquals(e.getErrors().get("recipient_id").get(0), "does not exist");
            throw e;
        }

    }
}
