<html>
    <body>
        <h2>Add Recipient</h2>
        <form action="" method="POST">
            Name: <input type="text" name="name">
            <input type="submit" name="create_recipient" value="Add">
            <#if create_recipient_invalid_name??><div>Please enter a name containing only letters and numbers</div></#if>
        </form>

        <br>

        <h2>Create Payment</h2>
        <form action="" method="POST">
            Recipient:
            <select name="recipient_id">
                <#list recipients as recipient>
                    <option value="${recipient.id}"
                        <#if created_recipient?? && created_recipient.id == recipient.id>
                            selected
                        </#if>
                    >
                    ${recipient.name}
                    </option>
                </#list>
            </select>
            &nbsp;&nbsp;
            Amount: <input type="text" name="amount">
            <select name="currency">
                <option value="GBP">GBP</option>
            </select>
            &nbsp;&nbsp;
            <input type="submit" name="create_payment" value="Pay">
            <br>
            <#if create_payment_amount_invalid??>
                Please enter a valid amount.
            </#if>
            <#if create_payment_errors??>
                <br>
                <#list create_payment_errors?keys as key>
                    ${create_payment_errors[key]?join(",")}
                </#list>
            </#if>
        </form>


        <br>

        <h2>Payment History</h2>
        <table>
            <tr>
                <th>Payment Id</th>
                <th>Recipient Id</th>
                <th>Recipient Name</th>
                <th>Amount</th>
                <th>Currency</th>
                <th>Status</th>
            <tr>
            <#list payments as payment>
                <tr>
                    <td>${payment.id}</td>
                    <td>${payment.recipientId}</td>
                    <td>${recipients_by_id[payment.recipientId].name}</td>
                    <td>${payment.amount}</td>
                    <td>${payment.currency}</td>
                    <td>${payment.status}</td>
                </tr>
            </#list>
        </table>

    </body>
</html>