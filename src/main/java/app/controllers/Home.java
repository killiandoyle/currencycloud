package app.controllers;

import app.Application;
import app.lib.Coolpay;
import app.models.User;
import org.glassfish.jersey.message.internal.XmlJaxbElementProvider;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.freemarker.FreeMarkerEngine;

import java.math.BigDecimal;
import java.util.*;


public class Home {
    public static Route route = (Request request, Response response) -> {
        String username = request.cookie("username");
        if (username == null || username.equals("")) {
            username = "KillianD";
            response.cookie("username", username);
        }

        User user = Application.userDao.getUserByUsername(username);
        if (user == null) throw new Exception("Invalid user");

        Coolpay coolpay = new Coolpay(user.getUsername(), user.getCoolpayApiKey());
        coolpay.setToken(user.getCoolpayToken());
        Map<String, Object> model = new HashMap<>();

        if (request.requestMethod().equals("POST")) {
            if (request.queryParams("create_recipient") != null) {
                String name = request.queryParams("name");
                name = name == null ? "" : name.trim();
                if (name.matches("^[a-zA-Z0-9]{1,}$")) {
                    Coolpay.Recipient recipient = coolpay.createRecipient(name);
                    model.put("created_recipient", recipient);
                } else {
                    model.put("create_recipient_invalid_name", true);
                }
            }

            if (request.queryParams("create_payment") != null) {
                String recipientId = request.queryParams("recipient_id");
                BigDecimal amount;
                try {
                    amount = new BigDecimal(request.queryParams("amount"));
                    if (amount.floatValue() < 0) throw new NumberFormatException();
                    String currency = request.queryParams("currency");
                    Coolpay.Payment payment = coolpay.createPayment(recipientId, amount, currency);
                    model.put("payment", payment);
                } catch (NumberFormatException e) {
                    model.put("create_payment_amount_invalid", true);
                } catch (Coolpay.EntityError e) {
                    model.put("create_payment_errors", e.getErrors());
                }
            }
        }

        List<Coolpay.Recipient> recipients = coolpay.listRecipients();
        Map<String, Coolpay.Recipient> recipientMap = new HashMap<>();
        for (Coolpay.Recipient recipient : recipients) recipientMap.put(recipient.getId(), recipient);
        List<Coolpay.Payment> payments = coolpay.listPayments();
        model.put("recipients", recipients);
        model.put("recipients_by_id", recipientMap);
        model.put("payments", payments);
        user.setCoolpayToken(coolpay.getToken());
        return new FreeMarkerEngine().render(
                new ModelAndView(model, "home.ftl")
        );
    };

    public static void checkAuth(Request request, Response response) {

    }
}
