package app;

import app.controllers.Home;
import app.dao.UserDao;
import spark.Spark;


import static spark.Spark.*;


public class Application {
    public static UserDao userDao = null;

    public static void main(String[] args) {
        userDao = new UserDao();
        port(4567);

        Spark.exception(Exception.class, (exception, request, response) -> {
            exception.printStackTrace();
        });

        get("*", Home.route);
        post("*", Home.route);
    }
}
