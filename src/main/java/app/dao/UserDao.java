package app.dao;


import jersey.repackaged.com.google.common.collect.ImmutableList;
import app.models.User;

import java.util.List;

public class UserDao {
    public class InvalidUserError extends Exception {}

    private final List<User> users = ImmutableList.of(
            new User("KillianD", "B44C477DAE97B123")
    );

    public User getUserByUsername(String username) {
        return users.stream().filter(b -> b.getUsername().equals(username)).findFirst().orElse(null);
    }
}
