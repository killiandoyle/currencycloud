package app.models;


public class User {
    String username;
    String coolpayApiKey;
    String coolpayToken;

    public User(String username, String coolpayApiKey) {
        this.username = username;
        this.coolpayApiKey = coolpayApiKey;
        this.coolpayToken = null;
    }

    public String getUsername() {
        return username;
    }

    public String getCoolpayApiKey() {
        return coolpayApiKey;
    }

    public String getCoolpayToken() {
        return this.coolpayToken;
    }

    public void setCoolpayToken(String token) {
        this.coolpayToken = token;
    }
}
