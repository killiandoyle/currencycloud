package app.lib;

import com.google.gson.*;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;


public class Coolpay {
    public static class Recipient {
        String id;
        String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Payment {
        public enum Status {
            PROCESSING, PAID
        }

        String id;
        BigDecimal amount;
        String currency;
        String recipientId;
        Status status;

        public String getId() {
            return id;
        }

        public void setId() {
            this.id = id;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getRecipientId() {
            return recipientId;
        }

        public void setRecipientId() {
            this.recipientId = recipientId;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }
    }

    public class AuthorizationError extends Exception {}
    public class InvalidTokenError extends Exception {}
    public class EntityError extends Exception {
        Map<String, List<String>> errors;
        public EntityError(Map<String, List<String>> errors) {
            this.errors = errors;
        }

        public Map<String, List<String>> getErrors() {
            return this.errors;
        }
    }

    static String url = "https://coolpay.herokuapp.com/api/";

    String username, apiKey, token;
    Client client;
    private Gson gson;


    public Coolpay(String username, String apiKey) throws Exception {
        this.token = null;
        this.username = username;
        this.apiKey = apiKey;
        client = ClientBuilder.newClient();
        gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(Payment.Status.class, new JsonDeserializer<Payment.Status>() {
                    @Override
                    public Payment.Status deserialize(JsonElement arg0, Type arg1,
                                             JsonDeserializationContext arg2) throws JsonParseException {
                        String value = arg0.getAsString().toLowerCase();
                        if (value.equals("processing")) {
                            return Payment.Status.PROCESSING;
                        }
                        return Payment.Status.PAID;
                    }})
                .create();
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }


    private class LoginArg {
        public String username;
        public String apikey;
    }

    public String login() throws TimeoutException, AuthorizationError {
        LoginArg arg = new LoginArg();
        arg.username = username;
        arg.apikey = apiKey;
        WebTarget target = client.target(url + "login");
        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON_TYPE);
        Response response = builder.post(Entity.json(gson.toJson(arg)));
        if (response.getStatus() != 200) throw new AuthorizationError();
        this.token = new JsonParser().parse(response.readEntity(String.class))
                .getAsJsonObject().get("token").getAsString();
        return token;
    }


    private class CreateRecipientArg {
        public class ARecipient {
            public String name;
        }
        public ARecipient recipient = new ARecipient();
    }

    private class CreateRecipientResult {
        public Recipient recipient = new Recipient();
    }

    public Recipient createRecipient(String name) throws TimeoutException, AuthorizationError {
        while (true) {
            try {
                if (token == null) throw new InvalidTokenError();
                CreateRecipientArg arg = new CreateRecipientArg();
                arg.recipient.name = name;
                Response response = client.target(url + "recipients")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .header("Authorization", "Bearer " + token)
                        .post(Entity.json(gson.toJson(arg)));

                if (response.getStatus() == 401) {
                    this.token = null;
                    throw new InvalidTokenError();
                } else if (response.getStatus() != 201) {
                    throw new InternalError("Invalid status code");
                }
                String content = response.readEntity(String.class);
                return gson.fromJson(content, CreateRecipientResult.class).recipient;
            } catch (InvalidTokenError e) {
                login();
            }
        }
    }


    private class ListRecipientsResult {
        public List<Recipient> recipients;
    }

    public List<Recipient> listRecipients() throws TimeoutException, AuthorizationError {
        while (true) {
            try {
                if (token == null) throw new InvalidTokenError();
                Response response = client.target(url + "recipients")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .header("Authorization", "Bearer " + token)
                        .get();

                if (response.getStatus() == 401) {
                    this.token = null;
                    throw new InvalidTokenError();
                }
                String content = response.readEntity(String.class);
                return gson.fromJson(content, ListRecipientsResult.class).recipients;
            } catch (InvalidTokenError e) {
                login();
            }
        }
    }



    private class CreatePaymentArg {
        public class Payment {
            public String recipientId;
            public BigDecimal amount;
            public String currency;
        }
        public Payment payment = new Payment();
    }

    private class CreatePaymentResult {
        public Payment payment;
    }

    private class CreatePaymentError {
        public Map<String, List<String>> errors;
    }

    public Payment createPayment(String recipientId, BigDecimal amount, String currency)
            throws EntityError, TimeoutException, AuthorizationError {
        while (true) {
            try {
                if (token == null) throw new InvalidTokenError();
                CreatePaymentArg arg = new CreatePaymentArg();
                arg.payment.recipientId = recipientId;
                arg.payment.amount = amount;
                arg.payment.currency = currency;

                Response response = client.target(url + "payments")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .header("Authorization", "Bearer " + token)
                        .post(Entity.json(gson.toJson(arg)));
                if (response.getStatus() == 401) {
                    this.token = null;
                    throw new InvalidTokenError();
                } else if (response.getStatus() == 422) {
                    CreatePaymentError error = gson.fromJson(response.readEntity(String.class),
                            CreatePaymentError.class);
                    throw new EntityError(error.errors);
                } else if (response.getStatus() != 201) {
                    throw new InternalError();
                }
                String content = response.readEntity(String.class);
                return gson.fromJson(content, CreatePaymentResult.class).payment;
            } catch (InvalidTokenError e) {
                login();
            }
        }
    }


    private class PaymentsResult {
        public List<Payment> payments;
    }

    public List<Payment> listPayments() throws TimeoutException, AuthorizationError {
        while (true) {
            try {
                if (token == null) throw new InvalidTokenError();
                Response response = client.target(url + "payments")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .header("Authorization", "Bearer " + token)
                        .get();

                if (response.getStatus() == 401) {
                    this.token = null;
                    throw new InvalidTokenError();
                }
                String content = response.readEntity(String.class);
                return gson.fromJson(content, PaymentsResult.class).payments;
            } catch (InvalidTokenError e) {
                login();
            }
        }
    }
}
